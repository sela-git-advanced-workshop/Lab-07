# Advanced Git Workshop
Lab 07: Using Git Notes

---

# Tasks

 - Creating some notes
 
 - Managing notes
 
 - Working with namespaces

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab7
$ cd lab7
```

---

## Creating some notes

  - Let's create some notes for the last 3 commits:
```
$ git notes add -m "last commit git note" master
```
```
$ git notes add -m "middle commit git note" master~1
```
```
$ git notes add -m "first commit git note" master~2
```

---

## Managing notes

  - Let's see the notes in the log:
```
$ git log -n 5
```

  - Now remove the note from the middle commit:
```
$ git notes remove master~1
```

  - Inspect the log again:
```
$ git log -n 5
```

  - Let's inspect the notes for a specific commit:
```
$ git notes show master
```

---

## Working with namespaces

  - Let's add some notes using namespaces:
```
$ git notes --ref "test" add -f -m "first commit git note in test namespace" master
$ git notes --ref "test" add -f -m "last commit git note in test namespace" master~2
```

  - Let's inspect the log (you will not see the new notes):
```
$ git log -n 3
```

  - Let's inspect the log passing the flag --show-notes:
```
$ git log -n 3 --show-notes=test
```

---

# Cleanup

 - Remove the repository used during the lab:
 
```
$ cd ..
$ rm -rf lab7
```
